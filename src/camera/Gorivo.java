/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

import javafx.scene.Group;
import javafx.scene.SubScene;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import object.Spacecraft;

/**
 *
 * @author Marko
 */
public class Gorivo extends Group{
    private int pozX,pozY;
    Spacecraft brod;
    
    Group g1;
    Rectangle rec1u;
    Text s1u;
    double move;
    private boolean noMoreFuel=false;
    
    Group root;
    SubScene headUp;

    public Gorivo(int pozX, int pozY,Spacecraft b) {
        this.pozX = pozX;
        this.pozY = pozY;
        this.brod = b;
        root=new Group();
        gorivo();
         
        headUp=new SubScene(root, 300 , 30);
        
        getChildren().addAll(headUp);
        setTranslateX(pozX-152);
        setTranslateY(pozY-50);
        
        
    }
    
     private void gorivo(){
        
        g1 = new Group();
         Stop [] tacke=new Stop[]{
                new Stop(0.0, Color.RED),
                new Stop(1.0, Color.GREEN)
        };
        LinearGradient lg=new LinearGradient(0, 0.5, 1, 0.5, true, CycleMethod.NO_CYCLE, tacke);
        Rectangle recu=new Rectangle(300, 20);
        
        //recu.setTranslateX(WINDOW_WIDTH/2-152);
        //recu.setTranslateY(WINDOW_HEIGHT-40);
       // recu.setTranslateZ(-300);
        recu.setFill(lg);
       // Rectangle rec1u;
        rec1u = new Rectangle(4 , 30,new Color(1, 1, 1, 0.65));
        //double move;
        move = 300*brod.getFuel()/brod.getMaxFuel();
        System.out.println("qwe   F="+brod.getFuel()+" MF="+brod.getMaxFuel()+"  >"+move);
        rec1u.setTranslateX(move);//
        rec1u.setTranslateY(-5);
        
        s1u = new Text((int)(brod.getFuel())+"/"+(int)(brod.getMaxFuel()));
        s1u.setTranslateY(-2);
        s1u.setFill(Color.WHITE);
        
        g1.getChildren().addAll(recu,rec1u,s1u);
        
        getChildren().addAll(g1);
    }
    
    public void update(){
        //System.out.println("qwe   F="+brod.getFuel()+" MF="+brod.getMaxFuel()+"  >"+move);
        move = 300*brod.getFuel()/brod.getMaxFuel();
        if(brod.getFuel()>0 && !noMoreFuel){
            rec1u.setTranslateX(move);
            s1u.setText((int)(brod.getFuel())+"/"+(int)(brod.getMaxFuel()));
        }else{
            noMoreFuel=true;
            s1u.setText("0/"+(int)(brod.getMaxFuel()));
            System.out.println(" GAme OVer");
        }
    
    }

    public boolean isNoMoreFuel() {
        return noMoreFuel;
    }
    
  

    public Spacecraft getBrod() {
        return brod;
    }

    public void setBrod(Spacecraft brod) {
        this.brod = brod;
    }
     
}