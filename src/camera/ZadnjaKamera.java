/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

import javafx.scene.Group;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

import object.Spacecraft;

/**
 *
 * @author pm163116
 */
public class ZadnjaKamera extends AbstractCamera {

    private double hor=-1000,ver=-100;
    Spacecraft brod;
    
   
    public ZadnjaKamera(Spacecraft b) {
        camera.getTransforms().setAll(new Rotate(-90, Rotate.X_AXIS));
        camera.getTransforms().addAll(new Rotate(-90, Rotate.Y_AXIS), new Translate(0, -100, -1000));
        brod=b;
        System.out.println("X="+ brod.getPosition().getX()+ " Y="+ brod.getPosition().getY()+" Z="+ brod.getPosition().getZ());
    }

    public void postaviHOR(double x){
        hor+=x;
    }
    
    public void postaviVER(double x){
        ver+=x;
    }
    
    
    @Override
    public void update() {
        double x= brod.getPosition().getX();
        double y= brod.getPosition().getY();
        double z= brod.getPosition().getZ();
        //brod.getRotationFactor()
       
        camera.getTransforms().setAll(new Translate(x, y, z),new Rotate(-90, Rotate.X_AXIS),new Rotate(-brod.getHorizontalAngle(), Rotate.Y_AXIS));
        camera.getTransforms().addAll( new Translate(0, ver, hor));
        
        
    }

    
}

