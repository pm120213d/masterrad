/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

import javafx.scene.Group;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author Marko
 */
public class FinisGame extends Group{
    private int sirina,visina;
    
    private int score;
    private int totalTime;
    
    boolean gameOver;
    
    Group root;
    SubScene finc;
    Text text,text2,text3;

    public FinisGame(int sirina, int visina,int score,boolean gameover) {
        this.sirina = sirina;
        this.visina = visina;
        this.score=score;
        root=new Group();
        
        setUpSceen();
        finc=new SubScene(root, sirina , visina,true,SceneAntialiasing.BALANCED);
        finc.setFill(Color.AQUAMARINE);
       
       getChildren().addAll(finc);
    }

    
    
    
    public void setUpSceen(){
        
        text = new  Text(0, visina/3, "Time`s up!");
        text.setFont(Font.font("Arial", 45));
        text.setTranslateX(sirina/2-text.getLayoutBounds().getWidth()/2);
        
        text2 = new  Text(0, visina/2, "Total play time : "+totalTime+ " sek");
        text2.setFont(Font.font("Arial", 45));
        text2.setTranslateX(sirina/2-text2.getLayoutBounds().getWidth()/2);
        
        text3 = new  Text(0, visina*2/3, "Your score : "+score);
        text3.setFont(Font.font("Arial", 45));
        text3.setTranslateX(sirina/2-text3.getLayoutBounds().getWidth()/2);
        
        root.getChildren().addAll(text,text2,text3);
        
    }
    
    public void update(int ttime,int lefttime,int sc,boolean go){
        totalTime=ttime-lefttime;
        score=sc;
        gameOver=go;
        
        text2.setText("Total play time : "+totalTime+ " sek");
        text3.setText("Your score : "+(int)score);
        if(gameOver){
            text.setText("GAME OVER");
            finc.setFill(Color.RED);
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTotalTime() {
        return totalTime;
    }

   
    
    
    
    
}
