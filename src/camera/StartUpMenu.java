/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.scene.Group;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;
import object.Spacecraft;

/**
 *
 * @author marko.pekovic
 */
public class StartUpMenu extends Group{
    private int sirina,visina;
    Spacecraft brod1,brod2,brod3;
    Spacecraft selectedRocket;
    boolean izabrana;
    
    Group root;
    SubScene startUP;

    public StartUpMenu(int sirina, int visina, Spacecraft brod1, Spacecraft brod2, Spacecraft brod3) {
        this.sirina = sirina;
        this.visina = visina;
        this.brod1 = brod1;
        this.brod2 = brod2;
        this.brod3 = brod3;
        root=new Group();
        createScene();
        startUP=new SubScene(root, sirina , visina,true,SceneAntialiasing.BALANCED);
        startUP.setFill(Color.BURLYWOOD);
        izabrana=false;
        
        brod1.setOnMousePressed(d -> {
                mousePressed(d,brod1);
            });
        brod2.setOnMousePressed(d -> {
                mousePressed(d,brod2);
            });
        brod3.setOnMousePressed(d -> {
                mousePressed(d,brod3);
            });
        getChildren().addAll(startUP);
       // setTranslateX(sirina/2);
        //setTranslateY(visina/2);
    }

    private void mousePressed(MouseEvent d,Spacecraft r) {
        
////        r.setRotationAxis(Rotate.Y_AXIS);
////        r.setRotate(90);
//        r.setRotationAxis(Rotate.Z_AXIS);
//        r.setRotate(-90);
        selectedRocket=r;
        izabrana=true;
    }
    
    private void prikazRakete(Spacecraft brod,double pozX,double pozY,double sirina, double visina,double scaleFact){
        
             /*PointLight pl1=new PointLight(Color.WHITE);
        pl1.setTranslateX(150);
        pl1.setTranslateY(300);
        pl1.setTranslateZ(-10);*/
//        brod1.setRotationAxis(Rotate.X_AXIS);
//        brod1.setRotate(90);
       
        Group rak1=new Group();
        Rectangle okvir=new Rectangle(sirina, visina );
        okvir.setFill(null);
        okvir.setStroke(new Color(1, 0.5, 0.5, 0.25));
        okvir.setStrokeWidth(5);
        
        Group radar=new Group(brod);
        radar.setTranslateX(sirina/2);//sirina/6);
        radar.setTranslateY(visina/3);//300);
        radar.getTransforms().add(new Scale(scaleFact, scaleFact, scaleFact));//(0.75, 0.75,0.75));
        
        RotateTransition rt=new RotateTransition(Duration.seconds(3),radar);
        rt.setAxis(Rotate.Y_AXIS);
        rt.setByAngle(360);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.play();
        
        Stop [] tacke=new Stop[]{
                new Stop(0.0, Color.GREEN),
                new Stop(1.0, Color.RED)
        };
        LinearGradient lg=new LinearGradient(0, 0.5, 1, 0.5, true, CycleMethod.NO_CYCLE, tacke);
        
        //text.getLayoutBounds().getWidth();
        Text sped=new Text("MaxSpeed");
        //sped.setFont(Font.font(20));
        //sped.setFont(Font.font("Arial", 15));
        sped.setTranslateX(sirina/4-sped.getLayoutBounds().getWidth());//sirina/9-sped.getLayoutBounds().getWidth());
        sped.setTranslateY(visina/2+100);
        
        Rectangle rec=new Rectangle(200, 15);
        rec.setTranslateX(sirina/4-sped.getLayoutBounds().getWidth());
        rec.setTranslateY(visina/2+110);
        rec.setFill(lg);
        Rectangle rec1=new Rectangle(5 , 25,new Color(0, 0, 0, 0.70));
        rec1.setTranslateX((sirina/4-sped.getLayoutBounds().getWidth())+200*brod.getMaxSpeed()/10000);
        rec1.setTranslateY(visina/2+105);
        
        Text s1=new Text((int)(brod.getMaxSpeed())+"/10000");
        s1.setTranslateX(sirina/4-sped.getLayoutBounds().getWidth()+200/2-s1.getLayoutBounds().getWidth()/2);
        s1.setTranslateY(visina/2+121);
        s1.setFill(Color.WHITE);
        
        //----------------------------------------------------
        
        Text up=new Text("Upravljivost");
        up.setTranslateX(sirina/4-sped.getLayoutBounds().getWidth());
        up.setTranslateY(visina/2+150);
        
        Rectangle recu=new Rectangle(200, 15);
        recu.setTranslateX(sirina/4-sped.getLayoutBounds().getWidth());
        recu.setTranslateY(visina/2+160);
        recu.setFill(lg);
        Rectangle rec1u=new Rectangle(5 , 25,new Color(0, 0, 0, 0.60));
        rec1u.setTranslateX((sirina/4-sped.getLayoutBounds().getWidth())+200*brod.getMaxRollAngle()/90);
        rec1u.setTranslateY(visina/2+155);
        
        Text s1u=new Text((int)(brod.getMaxPitchAngle())+"/90");
        s1u.setTranslateX(sirina/4-sped.getLayoutBounds().getWidth()+200/2-s1.getLayoutBounds().getWidth()/2);
        s1u.setTranslateY(visina/2+171);
        s1u.setFill(Color.WHITE);
        
        rak1.getChildren().addAll(okvir,radar,sped,up,rec,s1,rec1,recu,s1u,rec1u);
        rak1.setTranslateX(pozX);
        rak1.setTranslateY(pozY);
        root.getChildren().addAll(rak1);
    }
    
    
    private void createScene(){
        prikazRakete(brod1,0.0,0.0, sirina/3, visina, 0.75);
        prikazRakete(brod2,sirina/3,0.0, sirina/3, visina, 0.65);
        prikazRakete(brod3,sirina/3*2,0.0, sirina/3, visina, 0.65);
        
        
    }
    
    public Spacecraft selectedRocKet(){
        return selectedRocket;
    }

    public Spacecraft getBrod1() {
        return brod1;
    }

    public Spacecraft getBrod2() {
        return brod2;
    }

    public Spacecraft getBrod3() {
        return brod3;
    }

    public boolean isIzabrana() {
        return izabrana;
    }
    
    
}
