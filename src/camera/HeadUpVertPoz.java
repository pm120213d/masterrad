/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

import javafx.scene.Group;
import javafx.scene.SubScene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import object.Spacecraft;

/**
 *
 * @author marko.pekovic
 */
public class HeadUpVertPoz extends Group{
    private int sirina,visina;
    Spacecraft brod;
    
    
    Rectangle r,c1;
    Circle raketa;
    
    Group root;
    SubScene headUp;

    public HeadUpVertPoz(int sirina, int visina,Spacecraft b) {
        this.sirina = sirina/8;
        this.visina = visina;
        this.brod = b;
        root=new Group();
        createScene();
         
        headUp=new SubScene(root, this.sirina , visina);
        headUp.setFill(new Color(0.6, 1, 1, 0.4));
        
       
        
        getChildren().addAll(headUp);
        //setTranslateX();
        setTranslateY(visina*3-visina);
        
        
    }
    
    private void createScene(){
        Group radar=new Group();
        
        c1=new Rectangle(30,5);
        c1.setTranslateX(-15);
        raketa=new Circle(5);
        raketa.setFill(Color.RED);
        
        radar.getChildren().addAll(c1,raketa);
        radar.setTranslateX(sirina/2);
        radar.setTranslateY(visina/2);
        
        
        root.getChildren().addAll(radar);
        
    }
    
    public void update(){
        
        
       // raketa.setTranslateX(brod.getPosition().getX()/3000);
        raketa.setTranslateY(-brod.getPosition().getZ()/300);
    }
    
   
     public void hideHeadUpVertPoz(){
         setVisible(!isVisible());
    }

    public Spacecraft getBrod() {
        return brod;
    }

    public void setBrod(Spacecraft brod) {
        this.brod = brod;
    }
     
}
