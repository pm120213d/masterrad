/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

import javafx.scene.Group;
import javafx.scene.SubScene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import object.Spacecraft;

/**
 *
 * @author marko.pekovic
 */
public class HeadUP extends Group{
    private int sirina,visina;
    Spacecraft brod;
    
    Text xt,yt,zt,st;
    Rectangle r;
    Circle c1,c2,c3,raketa;
    
    Group root;
    SubScene headUp;

    public HeadUP(int sirina, int visina,Spacecraft b) {
        this.sirina = sirina;
        this.visina = visina;
        this.brod = b;
        root=new Group();
        createScene();
         
        headUp=new SubScene(root, sirina , visina);
        headUp.setFill(new Color(0.6, 1, 1, 0.4));
        
       
        
        getChildren().addAll(headUp);
        setTranslateX(sirina*3-sirina);
        setTranslateY(visina*3-visina);
        
        
    }
    
    private void createScene(){
        Group radar=new Group();
        
        raketa=new Circle(5);
        raketa.setFill(Color.RED);
        
        Circle okvir=new Circle(32);
        okvir.setFill(null);
        okvir.setStroke(Color.RED);
        okvir.setStrokeWidth(2);
        r = new Rectangle(5, 64);
        r.setFill(Color.WHITE);
        r.setTranslateX(-2.5);
        r.setTranslateY(-32);
        Group kompas=new Group(r,okvir);
        kompas.setTranslateX(sirina/2.6);
        kompas.setTranslateY(-visina/2.9);
        
        
        c1=new Circle(2);
        c2=new Circle(visina/6);
        c2.setFill(null);
        c2.setStroke(Color.BLUE);
        c2.setStrokeWidth(2);
        c3=new Circle(visina/3);
        c3.setFill(null);
        c3.setStroke(Color.BLUE);
        c3.setStrokeWidth(2);
        
        radar.getChildren().addAll(c1,c2,c3,raketa,kompas);
        radar.setTranslateX(sirina/2);
        radar.setTranslateY(visina/2);
        xt=new Text(5, visina-50, "x - 0");
        yt=new Text(5, visina-30, "y - 0");
        zt=new Text(5, visina-10, "z - 0");
        
        st=new Text(sirina-40, visina-40, "0");
        
        root.getChildren().addAll(radar,xt,yt,zt,st);
        
    }
    
    public void update(int scor){
        
        xt.setText("x - "+(int) brod.getPosition().getX());
        yt.setText("y - "+(int)brod.getPosition().getY());
        zt.setText("z - "+(int)brod.getPosition().getZ());
    
        st.setText(""+scor);
        raketa.setTranslateX(brod.getPosition().getX()/3000);
        raketa.setTranslateY(brod.getPosition().getY()/3000);
    }
    
    public void kompasMove(int i){
         r.setRotate(r.getRotate()+i);
    }
    public void kompasRest(){
         r.setRotate(0);
    }
     public void hideHeadUp(){
         setVisible(!isVisible());
    }

    public Spacecraft getBrod() {
        return brod;
    }

    public void setBrod(Spacecraft brod) {
        this.brod = brod;
    }
     
}
