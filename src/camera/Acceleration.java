/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.scene.SubScene;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import object.Spacecraft;

/**
 *
 * @author marko.pekovic
 */
public class Acceleration extends Group{
    private int pozX,pozY;
    
    
    Group g1;
    Rectangle rec1u;
    Text s1u;
    
    String way;
    int time;
    
    Group root;
    SubScene headUp;

    public Acceleration(int pozX, int pozY,int time,String way) {
        this.pozX = pozX;
        this.pozY = pozY;
        this.time = time;
        this.way = way;
        root=new Group();
        gorivo();
         
        headUp=new SubScene(root, 300 , 30);
        
        getChildren().addAll(headUp);
        setTranslateX(pozX-152);
        setTranslateY(150);
        
        
    }
    
     private void gorivo(){
        
        g1 = new Group();
         Stop [] tacke=new Stop[]{
                new Stop(0.0, Color.RED),
                new Stop(1.0, Color.GREEN)
        };
        LinearGradient lg=new LinearGradient(0, 0.5, 1, 0.5, true, CycleMethod.NO_CYCLE, tacke);
        Rectangle recu=new Rectangle(300, 20);
        
        recu.setFill(lg);
       
        rec1u = new Rectangle(4 , 30,new Color(1, 1, 1, 0.65));
        //double move;
        //move = 300*brod.getFuel()/brod.getMaxFuel();
        
        rec1u.setTranslateX(300);//
        TranslateTransition tt=new TranslateTransition(Duration.seconds(time), rec1u);
        tt.setFromX(300); tt.setToX(0);
        tt.play();
        rec1u.setTranslateY(-5);
        
        s1u = new Text("Acceleration "+way);
        s1u.setStroke(Color.ORANGERED);
        s1u.setFont(Font.font("Arial", 25));
        s1u.setTranslateY(-2);
        s1u.setFill(Color.WHITE);
        
        g1.getChildren().addAll(recu,rec1u,s1u);
        
        getChildren().addAll(g1);
    }
    
    public void update(){
        
    }

}
