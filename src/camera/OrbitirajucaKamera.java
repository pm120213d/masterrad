/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

import javafx.scene.Group;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import object.SpaceObject;

/**
 *
 * @author marko.pekovic
 */
public class OrbitirajucaKamera extends AbstractCamera {

    private double hor = -1000, ver = -100;
    SpaceObject brod;

    private Rotate rx = new Rotate();
    private Rotate ry = new Rotate();
    private Rotate rz = new Rotate();
    private Translate t = new Translate();
    Group nosac;

    public OrbitirajucaKamera(SpaceObject b) {
        brod = b;

        nosac = new Group(camera);
        rx.setAxis(Rotate.X_AXIS);
        ry.setAxis(Rotate.Y_AXIS);
        rz.setAxis(Rotate.Z_AXIS);
        rx.setAngle(0);
        ry.setAngle(0);
        rz.setAngle(-180);
        t = new Translate(0, 0, -500);
        camera.getTransforms().addAll(t);
        nosac.getTransforms().addAll(rx, ry, rz);
        brod.getChildren().add(nosac);
    }

    public void updatePosition(double deltaX, double deltaY) {
        ry.setAngle(ry.getAngle() - deltaX * 1.0);
        rx.setAngle(rx.getAngle() + deltaY * 1.0);

    }

    public void updateR(double deltaY) {
        double temp = t.getZ() - deltaY * 0.5;
        if (temp < -220) {
            t.setZ(temp);            
        } else {
            t.setZ(-200);
        }

    }

    @Override
    public void update() {

        camera.getTransforms().setAll(t);
        nosac.getTransforms().setAll(rx, ry, rz);

        /*double x= brod.getPosition().getX();
        double y= brod.getPosition().getY();
        double z= brod.getPosition().getZ();
        
        nosac.getTransforms().setAll(new Translate(x, y, z),new Rotate(-90, Rotate.X_AXIS),new Rotate(-brod.getHorizontalAngle(), Rotate.Y_AXIS));
        nosac.getTransforms().addAll( new Translate(0, ver, hor));
         */
    }
}
