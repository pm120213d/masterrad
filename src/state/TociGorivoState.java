/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import javafx.scene.transform.Rotate;
import object.Spacecraft;

/**
 *
 * @author marko.pekovic
 */
public class TociGorivoState extends State{

    public TociGorivoState(Spacecraft spacecraft) {
        super(spacecraft);
        //spacecraft.getTransforms().setAll(new Rotate(-spacecraft.getHorizontalAngle(), Rotate.Y_AXIS));
       // spacecraft.getTransforms().setAll(new Rotate(90, Rotate.X_AXIS));
    }

    @Override
    public void update(double passed, int pitchDirection, int rollDirection, int accelerationDirection){
        //spacecraft.getTransforms().setAll(new Rotate(-spacecraft.getHorizontalAngle(), Rotate.Z_AXIS));
        spacecraft.setFuel((int)(spacecraft.getFuel()+5));
    }
    
    @Override
    public void spacePressed() {
        spacecraft.setState(new LaunchState(spacecraft));
    }
}
