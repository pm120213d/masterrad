/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import object.Spacecraft;

/**
 *
 * @author marko.pekovic
 */
public class LandingState extends State {

    private double liftOffSpeed = 500;

    private double rootang;

    private double rotateAngle = 0;

    public LandingState(Spacecraft spacecraft, double rootang) {
        super(spacecraft);
        this.rootang = rootang;
    }

   private boolean positionup=true;
    @Override
    public void update(double passed, int pitchDirection, int rollDirection, int accelerationDirection) {

        if (positionup) {

            if (Math.abs(spacecraft.getPosition().getY()) > 0) {
                double tek = liftOffSpeed * passed * rotateAngle / 90.;
                if (spacecraft.getPosition().getY() > 0) {
                    spacecraft.getPosition().setY(spacecraft.getPosition().getY() - tek);
                    if (spacecraft.getPosition().getY() < 0) {
                        spacecraft.getPosition().setY(0);
                    }
                } else { //(spacecraft.getPosition().getZ()< 0) 
                    spacecraft.getPosition().setY(spacecraft.getPosition().getY() + tek);
                    if (spacecraft.getPosition().getY() > 0) {
                        spacecraft.getPosition().setY(0);
                    }
                }
            }

            if (Math.abs(spacecraft.getPosition().getX()) > 0) {
                double tek = liftOffSpeed * passed * rotateAngle / 90.;
                if (spacecraft.getPosition().getX() > 0) {
                    spacecraft.getPosition().setX(spacecraft.getPosition().getX() - tek);
                    if (spacecraft.getPosition().getX() < 0) {
                        spacecraft.getPosition().setX(0);
                    }
                } else { //(spacecraft.getPosition().getZ()< 0) 
                    spacecraft.getPosition().setX(spacecraft.getPosition().getX() + tek);
                    if (spacecraft.getPosition().getX() > 0) {
                        spacecraft.getPosition().setX(0);
                    }
                }
            }
            
            if(spacecraft.getPosition().getX()==0 && spacecraft.getPosition().getY()==0)positionup=false;

        } else {

            if (Math.abs(spacecraft.getPosition().getZ()) > 0) {
                double tek = liftOffSpeed * passed * rotateAngle / 90.;
                if (spacecraft.getPosition().getZ() > 0) {
                    spacecraft.getPosition().setZ(spacecraft.getPosition().getZ() - tek);
                    if (spacecraft.getPosition().getZ() < 0) {
                        spacecraft.getPosition().setZ(0);
                    }
                } else { //(spacecraft.getPosition().getZ()< 0) 
                    spacecraft.getPosition().setZ(spacecraft.getPosition().getZ() + tek);
                    if (spacecraft.getPosition().getZ() > 0) {
                        spacecraft.getPosition().setZ(0);
                    }
                }
            }
        }
        

        double angle = spacecraft.getHorizontalAngle();
        // System.out.println("Z kordinata= "+spacecraft.getPosition().getZ()+" Rotation factor" +(-spacecraft.getHorizontalAngle()));
        spacecraft.getTransforms().setAll(new Translate(spacecraft.getPosition().getX(), spacecraft.getPosition().getY(), spacecraft.getPosition().getZ()), new Rotate(angle, Rotate.Z_AXIS));

        if (rotateAngle <= 90) {
            rotateAngle++;
        } else {
            if (Math.abs(spacecraft.getHorizontalAngle()) > 0) {
                if (spacecraft.getHorizontalAngle() > 0) {
                    spacecraft.setHorizontalAngle(spacecraft.getHorizontalAngle() - 0.5);
                    if (spacecraft.getHorizontalAngle() < 0) {
                        spacecraft.setHorizontalAngle(0);
                    }
                } else {
                    spacecraft.setHorizontalAngle(spacecraft.getHorizontalAngle() + 0.5);
                    if (spacecraft.getHorizontalAngle() > 0) {
                        spacecraft.setHorizontalAngle(0);
                    }
                }
            }
        }
        spacecraft.getTransforms().add(new Rotate(rotateAngle, Rotate.X_AXIS));

        if (Math.abs(spacecraft.getPosition().getZ()) <= 1 && Math.abs(spacecraft.getPosition().getY()) <= 1 && Math.abs(spacecraft.getPosition().getX()) <= 1) {
            spacecraft.getPosition().setZ(0);
            spacecraft.getTransforms().setAll(new Translate(spacecraft.getPosition().getX(), spacecraft.getPosition().getY(), spacecraft.getPosition().getZ()));

            spacecraft.getTransforms().add(new Rotate(rotateAngle, Rotate.X_AXIS));
            //spacecraft.getTransforms().setAll(new Rotate(-spacecraft.getHorizontalAngle(), Rotate.Y_AXIS));
            //spacecraft.setHorizontalAngle(0);
            spacecraft.setState(new TociGorivoState(spacecraft));
        }
    }

}
