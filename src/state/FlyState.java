package state;

import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import object.Spacecraft;

public class FlyState extends State {
    private double maxRollAngle;
    private double maxPitchAngle;
    private double rollSpeed;
    private double pitchSpeed;

    private double maxSpeed;
    private double minSpeed;

    private double rollAngle;
    private double pitchAngle;
    private double yawAngle;

    private boolean landing=false;
    
    private double speed;

    private double rotationFactor;

    public FlyState(Spacecraft spacecraft) {
        super(spacecraft);
        this.maxRollAngle = spacecraft.getMaxRollAngle();
        this.maxPitchAngle = spacecraft.getMaxPitchAngle();
        this.rollSpeed = spacecraft.getRollSpeed();
        this.pitchSpeed = spacecraft.getPitchSpeed();
        this.maxSpeed = spacecraft.getMaxSpeed();
        this.minSpeed = spacecraft.getMinSpeed();
        this.rotationFactor = spacecraft.getRotationFactor();
        
        speed = 1500;
    }

    @Override
    public void update(double passed, int pitchDirection, int rollDirection, int accelerationDirection) {
        this.maxSpeed = spacecraft.getMaxSpeed();
        
        updateSpeed(passed, accelerationDirection);
        pitchAngle = updateAngleValue(passed, pitchDirection, pitchAngle, pitchSpeed, maxPitchAngle);
        rollAngle = updateAngleValue(passed, rollDirection, rollAngle, rollSpeed, maxRollAngle);
        yawAngle = rollAngle / 3;
        
        spacecraft.setFuel((int)(spacecraft.getFuel()-speed/10000));
        
        double horizontalSpeed = Math.cos(Math.toRadians(pitchAngle)) * speed;
        double verticalSpeed = Math.sin(Math.toRadians(pitchAngle)) * speed;
        spacecraft.getPosition().add(Math.cos(Math.toRadians(spacecraft.getHorizontalAngle() + 90)) * horizontalSpeed * passed, Math.sin(Math.toRadians(spacecraft.getHorizontalAngle() + 90)) * horizontalSpeed * passed, verticalSpeed * passed);
        spacecraft.setHorizontalAngle(spacecraft.getHorizontalAngle() - yawAngle * rotationFactor * passed * (speed > 0 ? horizontalSpeed / speed : 1));

        spacecraft.getTransforms().setAll(new Translate(spacecraft.getPosition().getX(), spacecraft.getPosition().getY(), spacecraft.getPosition().getZ()));
        spacecraft.getTransforms().addAll(new Rotate(spacecraft.getHorizontalAngle() - yawAngle, Rotate.Z_AXIS), new Rotate(pitchAngle, Rotate.X_AXIS), new Rotate(rollAngle, Rotate.Y_AXIS));
        
        //System.out.println("KORDINATEE   x="+   Math.abs(spacecraft.getPosition().getX())+"  y="+ Math.abs(spacecraft.getPosition().getY()) +" z="+ Math.abs(spacecraft.getPosition().getZ()));
        
        if(speed<500 &&  Math.abs(spacecraft.getPosition().getX())<10000 && Math.abs(spacecraft.getPosition().getY())<10000 && spacecraft.getPosition().getZ()<10000 && spacecraft.getPosition().getZ()>0){
            landing=true;
        }else{
            landing=false;
        }
        
    }

    private void updateSpeed(double passed, int acceleration) {
        if (acceleration == 0) return;
        double delta = speed * Math.exp(-(speed / maxSpeed - 1)) * passed;
        if (acceleration == 1) {
            speed = speed == 0 ? 100 : speed;
            speed = speed + delta > maxSpeed ? maxSpeed : speed + delta;
            
        } else {
            speed = speed - delta < minSpeed ? minSpeed : speed - delta;
        }
        
        //System.out.println("Brzina rakte je : "+speed);
    }

    private double updateAngleValue(double passed, int direction, double angle, double angleChangeSpeed, double maxAngle) {
        if (speed == 0) {
            angle = 0;
        } else if (direction == 1 || (direction == 0 && angle < 0)) {
            angle += angleChangeSpeed * passed;
            if (direction == 1 && angle > maxAngle) {
                angle = maxAngle;
            } else if (direction == 0 && angle > 0) {
                angle = 0;
            }
        } else if (direction == -1 || (direction == 0 && angle > 0)) {
            angle -= angleChangeSpeed * passed;
            if (direction == -1 && angle < -maxAngle) {
                angle = -maxAngle;
            } else if (direction == 0 && angle < 0) {
                angle = 0;
            }
        }

        if (speed / maxSpeed < 0.5) {
            if (Math.abs(angle / maxAngle) > (speed / maxSpeed) * 2.) {
                double c = angle > 0 ? 1 : -1;
                angle = c * (speed / maxSpeed) * 2. * maxAngle;
            }
        }

        return angle;
    }

    @Override
    public double getSpeed() {
        return speed;
    }

    @Override
    public void setSpeed(double speed) {
        this.speed = speed;
    }
    
    public boolean isLanding(){
        return landing;
    }
     
    
    @Override
     public void spaceB(){
         if(landing){
             spacecraft.setState(new LandingState(spacecraft,rollAngle));
         }
         
     }

    @Override
    public double getRollAngle() {
        return rollAngle;
    }
     
}
