package state;

import object.Spacecraft;

public abstract class State {
    protected Spacecraft spacecraft;

    public State(Spacecraft spacecraft) {
        this.spacecraft = spacecraft;
    }

    public void update(double passed, int pitchDirection, int rollDirection, int accelerationDirection){}
    public void spacePressed(){}
    public void spaceB(){}
    
    
    public void setSpeed(double speed) {} //ja sam dodao 
    public double getSpeed() {return 0; }
    public double getRollAngle() {
        return 0;
    }
    
    public boolean isLanding(){return false;}
}
