package object;

import java.util.Random;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

public class SpaceBubble extends SpaceObject {

    public SpaceBubble(double radius, Color diffuseColor) {
        PhongMaterial material = new PhongMaterial();
        Image image1 = new Image("resources/bump.jpg");

        material.setDiffuseColor(diffuseColor);
        material.setBumpMap(image1);

        Sphere sphere = new Sphere(radius);
        sphere.setMaterial(material);

        Group gr2 = new Group();

        for (int i = 0; i < 3; i++) {
            Sphere sp1 = new Sphere(radius / 10);
            Sphere sp = new Sphere(radius / 10);
            Random rnd = new Random();
            double u = Math.random();
            double v = Math.random();
            double theta = 2 * Math.PI * u;
            double phi = Math.acos(2 * v - 1);
            sp.setTranslateX(radius * Math.sin(phi) * Math.cos(theta));
            sp.setTranslateY(radius * Math.sin(phi) * Math.sin(theta));
            sp.setTranslateZ(radius * Math.cos(phi));
            
            sp1.setTranslateX(-radius * Math.sin(phi) * Math.cos(theta));
            sp1.setTranslateY(-radius * Math.sin(phi) * Math.sin(theta));
            sp1.setTranslateZ(-radius * Math.cos(phi));
            
            sp.setMaterial(material);
            sp1.setMaterial(material);

            gr2.getChildren().addAll(sp1,sp);
            
            RotateTransition rt=new RotateTransition(Duration.seconds(1),gr2);
            //TranslateTransition rt=new TranslateTransition(Duration.seconds(1),gr2);
            //rt.setAxis(Rotate.Y_AXIS);
           /* switch (i) {
            case 0:
                //sp.setTranslateZ(radius );
                //sp1.setTranslateZ(-radius );
                rt.setAxis(Rotate.X_AXIS);break;
            case 1:
                //sp.setTranslateX(radius );
                //sp1.setTranslateX(-radius );
                //sphere.setTranslateY(radius);
                rt.setAxis(Rotate.Y_AXIS);break;
            case 2:
                //sp.setTranslateY(radius );
                //sp1.setTranslateY(-radius );
                rt.setAxis(Rotate.Z_AXIS);break;
        }*/
            rt.setByAngle(360);
            //rt.setFromX(v);
            rt.setInterpolator(Interpolator.LINEAR);
            rt.setCycleCount(Animation.INDEFINITE);
            rt.play();
        }
        
       Group gr = new Group();

        for (int i = 0; i < 50; i++) {
            Sphere sp = new Sphere(radius / 30);
            
            Random rnd = new Random();
            double u = Math.random();
            double v = Math.random();
            double theta = 2 * Math.PI * u;
            double phi = Math.acos(2 * v - 1);
            sp.setTranslateX(radius * Math.sin(phi) * Math.cos(theta));
            sp.setTranslateY(radius * Math.sin(phi) * Math.sin(theta));
            sp.setTranslateZ(radius * Math.cos(phi));

            sp.setMaterial(material);

            gr.getChildren().addAll(sp);

           /* RotateTransition rt=new RotateTransition(Duration.seconds(3),gr);
            rt.setAxis(Rotate.Z_AXIS);
            rt.setByAngle(360);
            rt.setInterpolator(Interpolator.LINEAR);
            rt.setCycleCount(Animation.INDEFINITE);
            rt.play();*/
        }

        this.getChildren().addAll(sphere, gr,gr2);

    }

    private Group kruzeciMehur(Double radius, PhongMaterial material, int i) {
        Group gr2 = new Group();

        Sphere sphere = new Sphere(radius / 10);
        sphere.setMaterial(material);

        double u = Math.random();
        double v = Math.random();
        double theta = 2 * Math.PI * u;
        double phi = Math.acos(2 * v - 1);
        sphere.setTranslateX(radius * Math.sin(phi) * Math.cos(theta));
        sphere.setTranslateY(radius * Math.sin(phi) * Math.sin(theta));
        sphere.setTranslateZ(radius * Math.cos(phi));

        gr2.getChildren().addAll(sphere);
        gr2.setTranslateX(radius);
        RotateTransition rt = new RotateTransition(Duration.seconds(3), gr2);
        rt.setAxis(Rotate.Y_AXIS);
        /*switch (i) {
            case 1:
                //sphere.setTranslateX(radius);
                rt.setAxis(Rotate.X_AXIS);break;
            case 2:
                //sphere.setTranslateY(radius);
                rt.setAxis(Rotate.Y_AXIS);break;
            case 3:
                //sphere.setTranslateY(radius);
                rt.setAxis(Rotate.Z_AXIS);break;
        }*/
        rt.setByAngle(360);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.play();

        return gr2;

    }

}
