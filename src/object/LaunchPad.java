package object;

import javafx.scene.Group;
import javafx.scene.PointLight;


import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.*;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public class LaunchPad extends SpaceObject {

    PointLight l1,l2,l3;
    
    PhongMaterial ml1;
    
    public LaunchPad() {

        MeshView mv = makePlatform();
        makeLight();
        mv.setTranslateY(-25);
        //PhongMaterial matv=new PhongMaterial();
        Group stalak=stalak(10, 7,ml1);
        //stalak.setTranslateZ(50);
        stalak.getTransforms().add(new Translate(0, 150, 55));
        this.getChildren().addAll(mv,stalak);
        this.getTransforms().add(new Translate(0, 0, -200));
    }
    
    public void makeLight(){
        l1=new PointLight(Color.WHITE);
        l1.setLightOn(false);
        l1.setTranslateZ(160);
        l1.setTranslateX(780);
        l1.setTranslateY(-25);
        
        l2=new PointLight(Color.WHITE);
        l2.setLightOn(false);
        l2.setTranslateZ(160);
        l2.setTranslateX(-800);
        l2.setTranslateY(-25);
        
        l3=new PointLight(Color.WHITE);
        l3.setLightOn(false);
        l3.setTranslateZ(160);
        l3.setTranslateY(-800);
        //l3.setTranslateY(-25);
        
        
        ml1 = new PhongMaterial();
        ml1.setDiffuseColor(Color.WHITE);
        
        Sphere sv3=new Sphere(15);
        Cylinder cl3 = new Cylinder(4, 150);
        cl3.getTransforms().add(new Rotate(90, Rotate.X_AXIS));     
        cl3.setTranslateZ(75);
        sv3.setMaterial(ml1);
        sv3.setTranslateZ(160);
        Group lampa3=new Group(sv3,cl3);
        lampa3.getTransforms().add(new Translate(-800, -25, 0));
        
        Sphere sv1=new Sphere(15);
        Cylinder cl1 = new Cylinder(4, 150);
        cl1.getTransforms().add(new Rotate(90, Rotate.X_AXIS));     
        cl1.setTranslateZ(75);
        sv1.setMaterial(ml1);
        sv1.setTranslateZ(160);
        Group lampa1=new Group(sv1,cl1);
        lampa1.getTransforms().add(new Translate(800, -25, 0));
        
        Sphere sv2=new Sphere(15);
        Cylinder cl2 = new Cylinder(4, 150);
        cl2.getTransforms().add(new Rotate(90, Rotate.X_AXIS));     
        cl2.setTranslateZ(75);
        sv2.setMaterial(ml1);
        sv2.setTranslateZ(160);
        Group lampa2=new Group(sv2,cl2);
        lampa2.getTransforms().add(new Translate(0, -800, 0));
        
        this.getChildren().addAll(lampa1,lampa2,lampa3);
    }

    public MeshView makePlatform() {
        float[] points = {
            0, -800, 0,
            -800, 0, 0,
            0, 800, 0,
            800, 0, 0
        };

        float[] textCoords = {
            0.5f, 0.0f,
            0.0f, 0.5f,
            0.5f, 1.0f,
            1.0f, 0.5f
        };
        int[] faces = {
            0, 0, 1, 1, 2, 2,
            0, 0, 2, 2, 1, 1,
            0, 0, 2, 2, 3, 3,
            0, 0, 3, 3, 2, 2,};
        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(points);
        mesh.getTexCoords().addAll(textCoords);
        mesh.getFaces().addAll(faces);

        MeshView meshView = new MeshView();
        meshView.setMesh(mesh);
        PhongMaterial material = new PhongMaterial();

        Image image1 = new Image("resources/launchpad.jpg");
        
        material.setDiffuseMap(image1);
        material.setSpecularColor(Color.WHITE);
        material.setSpecularPower(256);
        ///material.setSelfIlluminationMap(new Image("resources/bulb.png"));
        // material.setDiffuseColor(Color.AQUA);
        meshView.setMaterial(material);
        return meshView;
    }
    
    public void LightOFF(){
        if (!l1.isLightOn()) {
            l1.setLightOn(!l1.isLightOn());
            ml1.setSelfIlluminationMap(new Image("resources/bulb.png"));
            l2.setLightOn(!l2.isLightOn());
            l3.setLightOn(!l3.isLightOn());
            getChildren().addAll(l1,l2,l3);
        }else{
           l1.setLightOn(!l1.isLightOn());
           ml1.setSelfIlluminationMap(null);
           l2.setLightOn(!l2.isLightOn());
           l3.setLightOn(!l3.isLightOn());
           getChildren().removeAll(l1,l2,l3); 
        }   
    }
    
    public Group kocka(int debljina,PhongMaterial matkv ){
        Group g=new Group();
        
       
        Box kv=new Box(80, debljina, debljina);
        Box kv2=new Box(debljina, 80, debljina);
        Box kv3=new Box(80, debljina, debljina);
        Box kv4=new Box(debljina, 80, debljina);
        
        Box kv5=new Box(debljina, debljina, 50);
        Box kv6=new Box(debljina, debljina, 50);
        Box kv7=new Box(debljina, debljina, 50);
        Box kv8=new Box(debljina, debljina, 50);
        Box kv9=new Box(debljina, debljina, Math.sqrt((80*80+25*25)));
        Box kv10=new Box(debljina, debljina, Math.sqrt((80*80+25*25)));
        
        kv.setMaterial(matkv);
        kv.setTranslateY(-40);
        kv2.setMaterial(matkv);
        kv2.setTranslateX(-40);
        kv3.setMaterial(matkv);
        kv3.setTranslateY(40);
        kv4.setMaterial(matkv);
        kv4.setTranslateX(40);
        
        kv5.setMaterial(matkv);
        kv5.setTranslateX(40);
        kv5.setTranslateY(40);
        kv5.setTranslateZ(-25);
        kv6.setMaterial(matkv);
        kv6.setTranslateX(40);
        kv6.setTranslateY(-40);
        kv6.setTranslateZ(-25);
        kv7.setMaterial(matkv);
        kv7.setTranslateX(-40);
        kv7.setTranslateY(40);
        kv7.setTranslateZ(-25);
        kv8.setMaterial(matkv);
        kv8.setTranslateX(-40);
        kv8.setTranslateY(-40);
        kv8.setTranslateZ(-25);
        
        
        kv9.setTranslateX(40);
        kv9.setTranslateZ(-25);
        kv9.setRotate(60);
        kv9.setRotationAxis(Rotate.X_AXIS);
        kv9.setMaterial(matkv);
        kv10.setTranslateX(-40);
        kv10.setTranslateZ(-25);
        kv10.setRotate(60);
        kv10.setRotationAxis(Rotate.X_AXIS);
        kv10.setMaterial(matkv);
        g.getChildren().addAll(kv,kv2,kv3,kv4,kv5,kv6,kv7,kv8,kv9,kv10);
        return g;
    }
    
     private Group stalak(int num, int debljina,PhongMaterial matkv) {
        Group gr = new Group();
        int tempMove = 0;
        for (int i = 0; i < num; i++) {
            Group g = kocka(debljina,matkv);
            g.setTranslateZ(tempMove);
            gr.getChildren().addAll(g);
            tempMove += 50;
        }
        Group gp=new Group();
        Box kv=new Box(80, debljina, debljina);
        Box kv2=new Box(debljina, 80, debljina);
        Box kv3=new Box(80, debljina, debljina);
        Box kv4=new Box(debljina, 80, debljina);
        kv.setMaterial(matkv);
        kv.setTranslateY(-40);
        kv2.setMaterial(matkv);
        kv2.setTranslateX(-40);
        kv3.setMaterial(matkv);
        kv3.setTranslateY(40);
        kv4.setMaterial(matkv);
        kv4.setTranslateX(40);
        gp.getChildren().addAll(kv,kv2,kv3,kv4);
        gp.setTranslateZ(-50);
        gr.getChildren().addAll(gp,kosnici(debljina, matkv));
        return gr;
    }
     
     private Group kosnici(int debljina,PhongMaterial matkv){
        Group gr=new Group();
        
        Box ko=new Box(100* Math.sqrt(2),debljina, debljina);
        ko.setTranslateX(75);
        ko.setTranslateY(75);
        ko.setMaterial(matkv);
        ko.getTransforms().addAll(new Rotate(45,Rotate.Z_AXIS),new Rotate(45,Rotate.Y_AXIS));
        
        Box ko1=new Box(100* Math.sqrt(2),debljina, debljina);
        ko1.setTranslateX(75);
        ko1.setTranslateY(-75);
        ko1.setMaterial(matkv);
        ko1.getTransforms().addAll(new Rotate(-45,Rotate.Z_AXIS),new Rotate(45,Rotate.Y_AXIS));
        
        Box ko2=new Box(100* Math.sqrt(2),debljina, debljina);
        ko2.setTranslateX(-75);
        ko2.setTranslateY(-75);
        ko2.setMaterial(matkv);
        ko2.getTransforms().addAll(new Rotate(45,Rotate.Z_AXIS),new Rotate(-45,Rotate.Y_AXIS));
        
        Box ko3=new Box(100* Math.sqrt(2),debljina, debljina);
        ko3.setTranslateX(-75);
        ko3.setTranslateY(75);
        ko3.setMaterial(matkv);
        ko3.getTransforms().addAll(new Rotate(-45,Rotate.Z_AXIS),new Rotate(-45,Rotate.Y_AXIS));
        
        // drzaci rakete
        Box ko4=new Box(debljina, 90 ,debljina);
        ko4.setTranslateZ(200);
        ko4.setTranslateY(-90);
        ko4.setMaterial(matkv);
        
        Box ko5=new Box(debljina, 80 ,debljina);
        ko5.setTranslateZ(150);
        ko5.setTranslateY(-80);
        ko5.setMaterial(matkv);
        
        gr.getChildren().addAll(ko,ko1,ko2,ko3,ko4,ko5);
        
        return gr;
    }
}
