/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;

/**
 *
 * @author marko.pekovic
 */
public class Ubrzanje extends SpaceObject {

    public Ubrzanje(double radius, Color diffuseColor) {
        PhongMaterial material = new PhongMaterial();
       // Image image1 = new Image("resources/fule.PNG");
        
        material.setDiffuseColor(diffuseColor);
        //material.setDiffuseMap(image1);
        Group ub= ubrzanje(radius, material);
       
        RotateTransition rt=new RotateTransition(Duration.seconds(2), ub);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        rt.setAxis(Rotate.Z_AXIS);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.setCycleCount(Animation.INDEFINITE);
        
        rt.play();
        

        this.getChildren().addAll(ub);

    
    }
    private Group ubrzanje(double radius,PhongMaterial material){
        Cylinder c=new Cylinder(radius, 40);
        c.setMaterial(material);
        Text tx1=new Text("UP");
        tx1.setFont(Font.font(radius));
        tx1.getTransforms().add(new Rotate(180, Rotate.Z_AXIS));
        tx1.setTranslateY(25);
        tx1.setTranslateX(tx1.getLayoutBounds().getWidth()/2);
        tx1.setTranslateZ(-tx1.getLayoutBounds().getHeight()/4);
        
        tx1.getTransforms().add(new Rotate(-90, Rotate.X_AXIS));
        tx1.setStroke(Color.GREEN);
        tx1.setStrokeWidth(8);
        
        Text tx=new Text("UP");
        tx.setFont(Font.font(radius));
        
        tx.setTranslateY(-22);
        tx.setTranslateX(-tx.getLayoutBounds().getWidth()/2);
        tx.setTranslateZ(-tx.getLayoutBounds().getHeight()/4);
        tx.getTransforms().add(new Rotate(-90, Rotate.X_AXIS));
        tx.setStroke(Color.GREEN);
        tx.setStrokeWidth(8);
         
        Group ka=new Group(c,tx,tx1);
         ka.getTransforms().add(new Scale(4, 4, 4));
        return ka;
    }
    
}
