/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

/**
 *
 * @author Marko
 */
public class SpeedUp extends SpaceObject {

    public SpeedUp(double radius, Color diffuseColor) {
        PhongMaterial material = new PhongMaterial();
        //Image image1 = new Image("resources/t1.jpg");
        
        material.setDiffuseColor(diffuseColor);
       // material.setBumpMap(image1);
       

        Sphere sphere = new Sphere(radius);
        sphere.setMaterial(material);
        
      
        

        this.getChildren().addAll(sphere);

    }
}
