/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author marko.pekovic
 */

import geometry.Vector;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.*;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import state.IdleState;

public class Rocket1 extends Spacecraft {

    public Rocket1() {
        
        rocketSETup();
        
        position = new Vector(0, 0, 0);
        PhongMaterial material = new PhongMaterial(Color.DARKCYAN);
        PhongMaterial material1 = new PhongMaterial(Color.DARKCYAN);
        Image image1 = new Image("resources/t1.jpg");
        Image image3 = new Image("resources/t3.jpg");
        material.setDiffuseMap(image1);
        material1.setDiffuseMap(image3);

        Cylinder mainBody = new Cylinder(30, 60);
        mainBody.setMaterial(material);
        mainBody.setRotationAxis(Rotate.Y_AXIS);
        mainBody.setRotate(180);

        Cylinder c1 = new Cylinder(17, 15);
        c1.setMaterial(material);
        c1.setTranslateY(-138);
        this.getChildren().addAll(c1);

        Group cone1 = makeCone(10,30, 60, material1);//
        cone1.setTranslateY(60);
        cone1.setRotationAxis(Rotate.X_AXIS);
        cone1.setRotate(-90);
        this.getChildren().addAll(cone1);

        Group cone2 = makeCone(12,30, 100, material1);//
        cone2.setTranslateY(-80 );
        //cone2.setTranslateX(16);
        //cone2.setTranslateZ(-9);
        cone2.setRotationAxis(Rotate.X_AXIS);
        cone2.setRotate(90);
        this.getChildren().addAll(cone2);

        Group cone3 = makeCone(12, 22, 35, material1);
        cone3.setTranslateY(-157);
        //cone3.setTranslateZ(16);
        cone3.setRotationAxis(Rotate.X_AXIS);
        cone3.setRotate(-90);
        this.getChildren().addAll(cone3);
        
        Sphere sphere = new Sphere(10);
        sphere.setMaterial(material);
        sphere.setTranslateY(90);
        sphere.setRotationAxis(Rotate.X_AXIS);
        sphere.setRotate(90);

        Group wing1 = makeWing(material);
        Group wing2 = makeWing(material);
        Group wing3 = makeWing(material);

        wing1.getTransforms().addAll(new Rotate(-90, Rotate.X_AXIS),new Rotate(-90, Rotate.Z_AXIS),new Translate(10, -5, -110));
        wing2.getTransforms().addAll(new Rotate(-90, Rotate.X_AXIS),new Scale(-1, 1, 1), new Translate(10, 0,-110));
        wing3.getTransforms().addAll(new Rotate(-90, Rotate.X_AXIS), new Translate(10, 0, -110));

        this.getChildren().addAll(mainBody, sphere, wing1, wing2, wing3);

        state = new IdleState(this);

        this.getTransforms().addAll(new Translate(position.getX(), position.getY(), position.getZ()));
    }

    
     private void rocketSETup(){
        maxRollAngle = 60;
        maxPitchAngle = 60;
        
        rollSpeed = 100;
        pitchSpeed = 100;
        
        maxSpeed = 3000;
        minSpeed = 200;
        
        maxFuel=8000;
        fuel= 6500;
        
    }
    public void update(double passed) {
        state.update(passed, pitchDirection, rollDirection, accelerationDirection);
    }

    private Group makeCone(float radiusM,float radius, float height, PhongMaterial material) {
        Group cone = new Group();

        int numberOfTriangles = 360;

        float[] points = new float[numberOfTriangles * 3 * 2 * 3];
        float[] textCoords = {
                0.5f, 0,
                0, 1,
                1, 1
        };
        int[] faces = new int[numberOfTriangles * 3 * 2 *2];

        for (int i = 0; i < numberOfTriangles; i++) {
            int index = i * 18;
            points[index] = (float)Math.cos(Math.toRadians(i)) * radiusM;          //  x
            points[index + 1] =(float)Math.sin(Math.toRadians(i)) * radiusM;       //  y
            points[index + 2] = height / 2;                                        //  z            //0
            points[index + 3] = (float)Math.cos(Math.toRadians(i)) * radius;
            points[index + 4] = (float)Math.sin(Math.toRadians(i)) * radius;
            points[index + 5] = - height / 2;                                                       //1
            points[index + 6] = (float)Math.cos(Math.toRadians(i + 1)) * radius;
            points[index + 7] = (float)Math.sin(Math.toRadians(i + 1)) * radius;
            points[index + 8] = - height / 2;                                                       //2
            
            points[index + 9] = (float)Math.cos(Math.toRadians(i)) * radiusM;        //  x
            points[index + 10] = (float)Math.sin(Math.toRadians(i)) * radiusM;       //  y
            points[index + 11] = height / 2;                                         //  Z          //3
            points[index + 12] = (float)Math.cos(Math.toRadians(i+1)) * radius;
            points[index + 13] = (float)Math.sin(Math.toRadians(i+1)) * radius;
            points[index + 14] = - height / 2;                                                      //4
            points[index + 15] = (float)Math.cos(Math.toRadians(i + 1)) * radiusM;
            points[index + 16] = (float)Math.sin(Math.toRadians(i + 1)) * radiusM;
            points[index + 17] =  height / 2;                                                       //5
            
        }

        for (int i = 0; i < numberOfTriangles; i++) {
            int index = i * 12;
            
            faces[index] = i * 6;
            faces[index + 1] = 0;
            faces[index + 2] = i * 6 + 1;
            faces[index + 3] = 1;
            faces[index + 4] = i * 6 + 2;
            faces[index + 5] = 2;
            
            faces[index + 6] = i * 6 + 3;
            faces[index + 7] = 1;
            faces[index + 8] = i * 6 + 4;
            faces[index + 9] = 0;
            faces[index + 10] = i * 6 + 5;
            faces[index + 11] = 2;
        }

        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(points);
        mesh.getTexCoords().addAll(textCoords);
        mesh.getFaces().addAll(faces);

        MeshView meshView = new MeshView();
        meshView.setMesh(mesh);
        meshView.setMaterial(material);

        Cylinder circleM = new Cylinder(radiusM, 0.1);
        circleM.setMaterial(material);
        circleM.setTranslateZ( height / 2);
        circleM.setRotationAxis(Rotate.X_AXIS);
        circleM.setRotate(90);
        
        Cylinder circle = new Cylinder(radius, 0.1);
        circle.setMaterial(material);
        circle.setTranslateZ(- height / 2);
        circle.setRotationAxis(Rotate.X_AXIS);
        circle.setRotate(90);


        cone.getChildren().addAll(meshView, circle,circleM);

        return cone;
    }

private Group makeWing(PhongMaterial material) {
        Group wing = new Group();

         float[] points = {
            0, 0, 80,//0
            0, 0, 0,//1
            50, 0, 0,//2
            0, 10, 80,//3
            0, 10, 0,//4
            50, 10, 0//5
           
        };

        float[] textCoords = {
            0.0f, 0.0f,//0
            1.0f, 0.0f,//1
            0.0f, 1.0f//2
           // 1.0f, 1.0f
        };
        int[] faces = {
            0, 0, 2, 2, 1, 1,
            0, 0, 1, 1, 2, 2,
            
            3, 0, 5, 2, 4, 1,
            3, 0, 4, 1, 5, 2,
            
            0, 0, 3, 0, 5, 2,
            0, 0, 5, 2, 3, 0,
            0, 0, 2, 2, 5, 2,
            0, 0, 5, 2, 2, 2,
            
            0, 0, 4, 1, 3, 0,
            0, 0, 3, 0, 4, 1,
            0, 0, 4, 1, 1, 1,
            0, 0, 1, 1, 4, 1,
            
            1,1,2,2,5,2,
            1,1,5,2,2,2,
            5,2,4,1,1,1,
            5,2,1,1,4,1
            
//            1, 1, 2, 2, 3, 3,
//            1, 1, 3, 3, 2, 2
        };
        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(points);
        mesh.getTexCoords().addAll(textCoords);
        mesh.getFaces().addAll(faces);

        MeshView meshView = new MeshView();
        meshView.setMesh(mesh);
        
        meshView.setMaterial(material);
        wing.getChildren().addAll(meshView);

        return wing;
    }

    private MeshView circularSide(TriangleMesh circleMesh, PhongMaterial material, int numberOfTriangles) {
        float[] points = new float[numberOfTriangles * 2 * 3 * 3];
        float[] textCoords = {
                0.5f, 0,
                0, 1,
                1, 1
        };
        int[] faces = new int[numberOfTriangles * 2 * 3 * 2];

        for (int i = 0; i < numberOfTriangles; i++) {
            int index = i * 9;
            float x1 = circleMesh.getPoints().get(index + 3);
            float y1 = circleMesh.getPoints().get(index + 4);
            float x2 = circleMesh.getPoints().get(index + 6);
            float y2 = circleMesh.getPoints().get(index + 7);

            int index2 = i * 18;
            points[index2] = x1;
            points[index2 + 1] = y1;
            points[index2 + 2] = 5;
            points[index2 + 3] = x1;
            points[index2 + 4] = y1;
            points[index2 + 5] = -5;
            points[index2 + 6] = x2;
            points[index2 + 7] = y2;
            points[index2 + 8] = -5;

            points[index2 + 9] = x2;
            points[index2 + 10] = y2;
            points[index2 + 11] = -5;
            points[index2 + 12] = x2;
            points[index2 + 13] = y2;
            points[index2 + 14] = 5;
            points[index2 + 15] = x1;
            points[index2 + 16] = y1;
            points[index2 + 17] = 5;
        }

        for (int i = 0; i < numberOfTriangles * 2; i++) {
            int index = i * 6;
            faces[index] = i * 3;
            faces[index + 1] = 0;
            faces[index + 2] = i * 3 + 1;
            faces[index + 3] = 1;
            faces[index + 4] = i * 3 + 2;
            faces[index + 5] = 2;
        }

        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(points);
        mesh.getTexCoords().addAll(textCoords);
        mesh.getFaces().addAll(faces);

        MeshView meshView = new MeshView();
        meshView.setMesh(mesh);
        meshView.setMaterial(material);
        meshView.setCullFace(CullFace.NONE);

        return meshView;
    }

    private MeshView wingMesh(PhongMaterial material, int numberOfTriangles) {
        float diff = (float)90 / numberOfTriangles;

        float[] points = new float[numberOfTriangles * 3 * 3];
        float[] textCoords = {
                0.5f, 0,
                0, 1,
                1, 1
        };
        int[] faces = new int[numberOfTriangles * 3 * 2];

        for (int i = 0; i < numberOfTriangles; i++) {
            int index = i * 9;
            points[index] = 0;
            points[index + 1] = 0;
            points[index + 2] = 0;
            float x1 = (float)Math.cos(Math.toRadians(i * diff)) * 50;
            float y1 = (float)Math.sin(Math.toRadians(i * diff)) * 50;
            float z1 = 0;
            float x2 = (float)Math.cos(Math.toRadians((i + 1) * diff)) * 50;
            float y2 = (float)Math.sin(Math.toRadians((i + 1) * diff)) * 50;
            float z2 = 0;

            points[index + 3] = x1;
            points[index + 4] = y1;
            points[index + 5] = z1;
            points[index + 6] = x2;
            points[index + 7] = y2;
            points[index + 8] = z2;
        }

        for (int i = 0; i < numberOfTriangles; i++) {
            int index = i * 6;
            faces[index] = i * 3;
            faces[index + 1] = 0;
            faces[index + 2] = i * 3 + 1;
            faces[index + 3] = 1;
            faces[index + 4] = i * 3 + 2;
            faces[index + 5] = 2;
        }

        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(points);
        mesh.getTexCoords().addAll(textCoords);
        mesh.getFaces().addAll(faces);

        MeshView meshView = new MeshView();
        meshView.setMesh(mesh);
        meshView.setMaterial(material);
        meshView.setCullFace(CullFace.NONE);

        return meshView;
    }

}


