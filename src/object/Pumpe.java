/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;

/**
 *
 * @author Marko
 */
public class Pumpe extends SpaceObject {

    public Pumpe(double radius, Color diffuseColor) {
        PhongMaterial material = new PhongMaterial();
        Image image1 = new Image("resources/fule.PNG");
        
        //material.setDiffuseColor(diffuseColor);
        material.setDiffuseMap(image1);
       Group pum=kanta(material);
       
       RotateTransition rt=new RotateTransition(Duration.seconds(2), pum);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        rt.setAxis(Rotate.Z_AXIS);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.setCycleCount(Animation.INDEFINITE);
        
        rt.play();
        

        this.getChildren().addAll(pum);

    }
    private Group kanta(PhongMaterial material){
        Box k1=new Box(30, 100, 60);
        k1.setMaterial(material);
        Cylinder valjak= new Cylinder(5, 10, 10);
        valjak.setTranslateY(-50);
        valjak.setTranslateZ(-20);
        valjak.setMaterial(material);
        Box r1=new Box(10, 10, 5);
        r1.setMaterial(material);
        Box r2=new Box(10, 10, 5);
        r2.setMaterial(material);
        Box r3=new Box(10, 5, 25);
        r3.setMaterial(material);
        r1.setTranslateY(-55);
        r1.setTranslateZ(20);
        r2.setTranslateY(-55);
        r3.setTranslateY(-57);
        r3.setTranslateZ(10);
        Group ka=new Group(k1,valjak,r1,r2,r3);
        ka.getTransforms().addAll(new Scale(4, 4, 4),new Rotate(-90, Rotate.X_AXIS));
        
        return ka;
    }

}
