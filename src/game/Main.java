package game;

import camera.*;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import object.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import state.FlyState;

public class Main extends Application {

    private static final int WINDOW_HEIGHT = 700;
    private static final int WINDOW_WIDTH = 900;

    private Group oldG = null;
    private Text vreme;
    private Timeline tajmer;
    private int sec = 0, min = 0;
    private boolean timeISup = false;
    private int totalTime;
    private boolean firstClick = false;
    private Text accelText, landingText;

    private Group root;
    private Group mainSceneRoot;

    private Scene scene;
    private SubScene mainSubscene;
    private HeadUP headUp;
    private HeadUpVertPoz headVertPoz;
    private StartUpMenu startUp;
    private FinisGame finisGame;
    private Gorivo gorivo;
    private int accelNum=0;

    private Spacecraft spacecraft;
    private LaunchPad launchPad;
    private List<SpaceObject> usporenja = new ArrayList<>();
    private List<SpaceObject> ubrzanja = new ArrayList<>();
    private List<SpaceObject> pumps = new ArrayList<>();
    private List<SpaceObject> bubbles = new ArrayList<>();
    private int bubblesHit;

    private UpdateTimer timer = new UpdateTimer();

    private DefaultCamera defaultCamera = new DefaultCamera();
    private ZadnjaKamera zadnjaKamera;
    private OrbitirajucaKamera orbKamera;

    private AbstractCamera camera;

    private double mouseXRight;
    private double mouseYRight;

    private double OLDmouseXRight;
    private double OLDmouseYRight;

    private class UpdateTimer extends AnimationTimer {

        private long previous = 0;

        @Override
        public void handle(long now) {
            if (previous == 0) {
                previous = now;
            }
            float passed = (now - previous) / 1e9f;
            spacecraft.update(passed);
            camera.update();
            headUp.update(bubblesHit);
            headVertPoz.update();
            gorivo.update();
            accelText.setText("Acceleration:  x" + accelNum);
            if (accelNum > 0) {
                accelText.setStroke(Color.ORANGERED);
            } else {
                accelText.setStroke(Color.WHITE);
            }
            landingText.setVisible(spacecraft.isLanding());
            checkForCollisions();
            //checkForCollisionsPumps();
            checkForCollisionsPumpe();
            checkForCollisionsUbrzanja();
            checkForCollisionsUsporenja();
            if (startUp.isVisible()) {
                desioSeKlikUmeniju();
            }
            if (gorivo.isNoMoreFuel()) {// da probam da postavim bez ifa
                finisGame.setVisible(true);
                finisGame.update(totalTime, sec + min * 60, bubblesHit, true);
                timer.stop();
                tajmer.stop();
            }

            previous = now;
        }
    }

    private void desioSeKlikUmeniju() {
        if (startUp.isIzabrana()) {
            mainSceneRoot.getChildren().remove(spacecraft);
            spacecraft = startUp.selectedRocKet();
            startUp.setVisible(false);
            root.getChildren().remove(startUp);

            headUp.setBrod(spacecraft);
            headVertPoz.setBrod(spacecraft);
            gorivo.setBrod(spacecraft);

            mainSceneRoot.getChildren().addAll(spacecraft);
        }
    }

    private void checkForCollisions() {
        Iterator<SpaceObject> iterator = bubbles.iterator();
        while (iterator.hasNext()) {
            SpaceObject spaceObject = iterator.next();
            if (spaceObject.getBoundsInParent().intersects(spacecraft.getBoundsInParent())) {
                positionObject(spaceObject);
                bubblesHit++;
                addTime(15);
            }
        }
    }

    private void checkForCollisionsPumpe() {
        Iterator<SpaceObject> iterator = pumps.iterator();
        while (iterator.hasNext()) {
            SpaceObject spaceObject = iterator.next();
            if (spaceObject.getBoundsInParent().intersects(spacecraft.getBoundsInParent())) {
                positionObject(spaceObject);
                spacecraft.setFuel(spacecraft.getFuel() + 1000);
            }
        }
    }
    private void checkForCollisionsUbrzanja() {
        Iterator<SpaceObject> iterator = ubrzanja.iterator();
        while (iterator.hasNext()) {
            SpaceObject spaceObject = iterator.next();
            if (spaceObject.getBoundsInParent().intersects(spacecraft.getBoundsInParent())) {
                positionObject(spaceObject);
                accelNum++;
            }
        }
    }

    private void checkForCollisionsUsporenja() {
        Iterator<SpaceObject> iterator = usporenja.iterator();
        while (iterator.hasNext()) {
            SpaceObject spaceObject = iterator.next();
            if (spaceObject.getBoundsInParent().intersects(spacecraft.getBoundsInParent())) {
                positionObject(spaceObject);
                
                looc = true;
                //*-**-*-*-*-*-*-*-*-*-*-*-**-*-*--*---------------------------------------------
                Acceleration acc1 = new Acceleration(WINDOW_WIDTH / 2, WINDOW_HEIGHT, 4, "Down");
                root.getChildren().addAll(acc1);
                //spacecraft.setMaxSpeed(8888);
                spacecraft.setAccelerationDirection(-1);
                Timeline tl = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        //spacecraft.setMaxSpeed(6000);
                        spacecraft.setAccelerationDirection(-1);

                        Timeline tll = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                //if (spacecraft.getAccelerationDirection() == 1){
                                // spacecraft.setMaxSpeed(6000);
                                spacecraft.setAccelerationDirection(0);
                                //} 
                                root.getChildren().remove(acc1);
                                // accelNum--;
                                looc = false;
                                spacecraft.getState().setSpeed(1500);
                            }
                        }));
                        tll.play();
                    }
                }));
                tl.play();
            }
        }
    }

    private void drawBubble() {
        SpaceBubble bubble = new SpaceBubble(450, Color.LIGHTGREEN);
        Pumpe pumpa = new Pumpe(500, Color.YELLOW);
        Ubrzanje ub=new Ubrzanje(100, Color.LIGHTYELLOW);
        Usporenje us=new Usporenje(100, Color.LIGHTYELLOW);
        positionObject(bubble);
        positionObject(pumpa);
        positionObject(ub);
        positionObject(us);
        bubbles.add(bubble);
        pumps.add(pumpa);
        ubrzanja.add(ub);
        usporenja.add(us);
        mainSceneRoot.getChildren().addAll(bubble, pumpa,ub,us);
    }

    private void bubbleHide() {
        for (int i = 0; i < bubbles.size(); i++) {
            bubbles.get(i).setVisible(!bubbles.get(i).isVisible());
        }
    }

    private void positionObject(SpaceObject object) {
        double x = (Math.random() < 0.5 ? 1f : -1f) * (Math.random() * 20000. + 10000);
        double y = (Math.random() < 0.5 ? 1f : -1f) * (Math.random() * 20000. + 10000);
        double z = (Math.random() < 0.5 ? 1f : -1f) * (Math.random() * 20000. + 10000);
        object.setTranslateX(x);
        object.setTranslateY(y);
        object.setTranslateZ(z);
    }

    private void setUpSpaceObjects() {
        for (int i = 0; i < 12; i++) {
            drawBubble();
        }
    }

    private void createMainScene() {
        mainSceneRoot = new Group();
        mainSubscene = new SubScene(mainSceneRoot, WINDOW_WIDTH, WINDOW_HEIGHT, true, SceneAntialiasing.BALANCED);
        mainSubscene.setFill(Color.BLACK);

        spacecraft = new Rocket();// ovde cemo staviti raketu koja je izabrana iz pocetne scene selectedRocket; //
        launchPad = new LaunchPad();
        mainSceneRoot.getChildren().addAll(launchPad, spacecraft);
        setUpSpaceObjects();
        instantiateCameras();
        mainSubscene.setCamera(camera.getCamera());
    }

    private void instantiateCameras() {
        camera = defaultCamera;
    }

    private void addTime(int s) {
        totalTime += s;
        sec += s;
        if (sec > 60) {
            min++;
            sec -= 60;
        }
        if (sec <= 9) {
            vreme.setText("Time:    0" + min + " : 0" + sec);
        } else {
            vreme.setText("Time:    0" + min + " : " + sec);
        }
    }

    private Timeline timer() {
        Timeline tl = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                sec--;
                if (sec <= 9) {
                    vreme.setText("Time:    0" + min + " : 0" + sec);
                } else {
                    vreme.setText("Time:    0" + min + " : " + sec);
                }
                if (sec == 0) {
                    if (min == 0 && sec == 0) {
                        timeISup = true;
                        finisGame.setVisible(true);
                        finisGame.update(totalTime, sec + min * 60, bubblesHit, false);
                        timer.stop();
                        tajmer.stop();
                    }
                    sec = 60;
                    min--;
                }

            }
        }));
        tl.setCycleCount(Timeline.INDEFINITE);
        return tl;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        sec = 19;
        min = 5;
        totalTime = sec + min * 60;

        accelText = new Text(0, 35, "Acceleration:  x" + accelNum);
        accelText.setTranslateX(WINDOW_WIDTH * 4 / 6);

        accelText.setStrokeWidth(3);
        accelText.setFont(Font.font(35));

        landingText = new Text(2, 35, " Landing is possible! ");
        landingText.setStroke(Color.YELLOWGREEN);
        landingText.setStrokeWidth(3);
        landingText.setFont(Font.font(35));

        vreme = new Text(WINDOW_WIDTH / 3 + 35, 35, "Time:    0" + min + " : " + sec);
        vreme.setStroke(Color.WHITE);
        vreme.setStrokeWidth(3);
        vreme.setFont(Font.font(35));

        root = new Group();
        createMainScene();

        headUp = new HeadUP(WINDOW_WIDTH / 3, WINDOW_HEIGHT / 3, spacecraft);
        headVertPoz = new HeadUpVertPoz(WINDOW_WIDTH / 3, WINDOW_HEIGHT / 3, spacecraft);

        startUp = new StartUpMenu(WINDOW_WIDTH, WINDOW_HEIGHT, new Rocket(), new Rocket1(), new Rocket2());
        finisGame = new FinisGame(WINDOW_WIDTH, WINDOW_HEIGHT, 22, false);
        finisGame.setVisible(false);

        gorivo = new Gorivo(WINDOW_WIDTH / 2, WINDOW_HEIGHT, spacecraft);

        root.getChildren().addAll(mainSubscene, vreme, accelText, landingText, gorivo, headVertPoz, headUp, startUp, finisGame);

        scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT, true);
        scene.setOnKeyPressed(e -> onKeyPressed(e));
        scene.setOnKeyReleased(e -> onKeyReleased(e));
        scene.setOnMousePressed(e -> onMousePressed(e));
        scene.setOnMouseDragged(e -> onMouseDragged(e));
        primaryStage.setTitle("RAKETA");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
        timer.start();
    }

    private boolean looc = false;

    private void onKeyPressed(KeyEvent e) {
        switch (e.getCode()) {
            case UP:
                spacecraft.setPitchDirection(-1);
                break;
            case DOWN:
                spacecraft.setPitchDirection(1);
                break;
            case LEFT:
                spacecraft.setRollDirection(-1);
                headUp.kompasMove(-1);
                break;
            case RIGHT:
                spacecraft.setRollDirection(1);
                headUp.kompasMove(1);

                break;
            case C:
                if (!looc) {
                    spacecraft.setAccelerationDirection(1);
                }
                break;
            case X:
                if (!looc) {
                    spacecraft.setAccelerationDirection(-1);
                }
                break;
            case F: // acceleration
                if (!looc) {
                    if (accelNum > 0) {
                        looc = true;
                        Acceleration acc1 = new Acceleration(WINDOW_WIDTH / 2, WINDOW_HEIGHT, 4, "Up");
                        root.getChildren().addAll(acc1);
                        spacecraft.setMaxSpeed(8888);
                        spacecraft.setAccelerationDirection(1);
                        Timeline tl = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                spacecraft.setMaxSpeed(6000);
                                spacecraft.setAccelerationDirection(-1);
                                Timeline tll = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        spacecraft.setAccelerationDirection(0);
                                        root.getChildren().remove(acc1);
                                        accelNum--;
                                        looc = false;
                                        spacecraft.getState().setSpeed(1500);
                                    }
                                }));
                                tll.play();
                            }
                        }));
                        tl.play();
                    }
                }
                break;
            case H:
                headUp.hideHeadUp();
                headVertPoz.hideHeadUpVertPoz();
                break;
            case L:
                launchPad.LightOFF();
                bubbleHide();
                break;
            case DIGIT1:
                camera = defaultCamera;
                mainSubscene.setCamera(camera.getCamera());
                break;
            case DIGIT2:
                zadnjaKamera = new ZadnjaKamera(spacecraft);
                camera = zadnjaKamera;

                /* Group go=zadnjaKamera.gorivo();
                if(mainSceneRoot.getChildren().contains(oldG) ){
                    mainSceneRoot.getChildren().remove(oldG);
                }
                mainSceneRoot.getChildren().add(go);
                oldG=go;*/
                mainSubscene.setCamera(camera.getCamera());
                break;
            case DIGIT3:
                orbKamera = new OrbitirajucaKamera(spacecraft);
                camera = orbKamera;
                mainSubscene.setCamera(camera.getCamera());
                break;
            case PAGE_UP:
                zadnjaKamera.postaviVER(-50);
                break;
            case PAGE_DOWN:
                zadnjaKamera.postaviVER(50);
                break;
            case ADD:
                zadnjaKamera.postaviHOR(-50);
                break;
            case SUBTRACT:
                zadnjaKamera.postaviHOR(50);
                break;
//            case B:
//                spacecraft.spaceB();
//                headUp.kompasRest();
//                break;
            case SPACE:
                if (!firstClick) {
                    tajmer = timer();
                    tajmer.play();
                    firstClick = true;
                }
                spacecraft.spacePressed();
                if (spacecraft.isLanding()) {
                    headUp.kompasRest();
                }
                spacecraft.spaceB();
                break;
        }
    }

    private void onKeyReleased(KeyEvent e) {
        switch (e.getCode()) {
            case UP:
                if (spacecraft.getPitchDirection() == -1) {
                    spacecraft.setPitchDirection(0);
                }
                break;
            case DOWN:
                if (spacecraft.getPitchDirection() == 1) {
                    spacecraft.setPitchDirection(0);
                }
                break;
            case LEFT:
                if (spacecraft.getRollDirection() == -1) {
                    spacecraft.setRollDirection(0);
                }
                break;
            case RIGHT:
                if (spacecraft.getRollDirection() == 1) {
                    spacecraft.setRollDirection(0);
                }
                break;
            case C:
                if (spacecraft.getAccelerationDirection() == 1) {
                    spacecraft.setAccelerationDirection(0);
                }
                break;
            case X:
                if (spacecraft.getAccelerationDirection() == -1) {
                    spacecraft.setAccelerationDirection(0);
                }
                break;
        }
    }

    private void onMousePressed(MouseEvent e) {
        if (e.getButton().ordinal() == 3 || e.getButton().ordinal() == 1) {
            OLDmouseXRight = mouseXRight = e.getSceneX();
            OLDmouseYRight = mouseYRight = e.getSceneY();
        }
    }

    private void onMouseDragged(MouseEvent e) {
        if (camera == defaultCamera) {
            if (e.getButton().ordinal() == 3) {
                defaultCamera.updatePosition(e.getSceneX() - mouseXRight, e.getSceneY() - mouseYRight);

            }
        } else {
            if (camera == orbKamera) {
                if (e.getButton().ordinal() == 1) {
                    OLDmouseXRight = mouseXRight;
                    OLDmouseYRight = mouseYRight;
                    orbKamera.updatePosition(e.getSceneX() - OLDmouseXRight, e.getSceneY() - OLDmouseYRight);
                }
                if (e.getButton().ordinal() == 3) {
                    orbKamera.updateR(e.getSceneY() - OLDmouseYRight);
                }
            }
        }
        mouseXRight = e.getSceneX();
        mouseYRight = e.getSceneY();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
